import React, { useState, useEffect } from 'react';
import clsx from 'clsx';

import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import InputBase from '@material-ui/core/InputBase';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import FilterIcon from '@material-ui/icons/FilterList';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import SearchIcon from '@material-ui/icons/Search';
import DirectionsIcon from '@material-ui/icons/Directions';
import Input from '@material-ui/core/Input';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import FilledInput from '@material-ui/core/FilledInput';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import DraftsIcon from '@material-ui/icons/Drafts';
import SendIcon from '@material-ui/icons/Send';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import Checkbox from '@material-ui/core/Checkbox';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemLink from '@material-ui/core/ListItem';
import Icon from '@material-ui/core/Icon';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';

const useStyles = makeStyles({
  root: {
    padding: '2px 4px',
    display: 'flex',
    alignItems: 'center',
    width: '100%'
  },
  advRoot: {
    marginTop: '5px',
    padding: '2px 4px',
    display: 'flex',
    alignItems: 'center',
    width: '100%'
  },
  input: {
    marginLeft: 8,
    flex: 1
  },
  iconButton: {
    padding: 10
  },
  divider: {
    width: 1,
    height: 28,
    margin: 4
  }
});

const StyledMenu = withStyles({
  paper: {
    border: '1px solid #d3d4d5'
  }
})(props => (
  <Menu
    elevation={0}
    getContentAnchorEl={null}
    anchorOrigin={{
      vertical: 'bottom',
      horizontal: 'center'
    }}
    transformOrigin={{
      vertical: 'top',
      horizontal: 'center'
    }}
    {...props}
  />
));

const StyledMenuItem = withStyles(theme => ({
  root: {
    '&:focus': {
      backgroundColor: theme.palette.primary.main,
      '& .MuiListItemIcon-root, & .MuiListItemText-primary': {
        color: theme.palette.common.white
      }
    }
  }
}))(MenuItem);

export default function SearchInput(props) {
  function Enum(constantsList) {
    for (var i in constantsList) {
      this[constantsList[i]] = i;
    }
  }

  const SortByEnum = new Enum(['NAME', 'COST', 'HEALTH', 'SPEED']);
  const SortOrderEnum = new Enum(['ASC', 'DESC']);
  const SortRadioEnum = new Enum([
    'COST_HIGH',
    'COST_LOW',
    'NAME_HIGH',
    'NAME_LOW'
  ]);

  const classes = useStyles();
  const [type, setType] = React.useState({
    value: 10
  });

  const [showAdvBar, setAdvBar] = React.useState({
    show: false
  });

  const [selectedValue, setSelectedValue] = React.useState(
    SortRadioEnum.COST_HIGH
  );

  function handleSortChange(event) {
    const value = event.target.value;
    setSelectedValue(value);
    updateSortQuery(value);
  }

  function updateSortQuery(value) {
    console.log('New sort value', value);
    let sortBy = '';
    let sortOrder = '';
    switch (value) {
      case SortRadioEnum.COST_HIGH:
        sortBy = SortByEnum.COST;
        sortOrder = SortOrderEnum.DESC;
        break;
      case SortRadioEnum.COST_LOW:
        sortBy = SortByEnum.COST;
        sortOrder = SortOrderEnum.ASC;
        break;
      case SortRadioEnum.NAME_HIGH:
        sortBy = SortByEnum.NAME;
        sortOrder = SortOrderEnum.DESC;
        break;
      case SortRadioEnum.NAME_LOW:
        sortBy = SortByEnum.NAME;
        sortOrder = SortOrderEnum.ASC;
        break;
      default:
        sortBy = SortByEnum.COST;
        sortOrder = SortOrderEnum.DESC;
        break;
    }
    console.log('Setting searchBy', sortBy);
    console.log('Setting order', sortOrder);

    setQuery(prevState => ({
      ...prevState,
      sortOrder: sortOrder,
      searchBy: sortBy
    }));
  }

  function handleTypeChange(event) {
    let typeVal = event.target.value;
    let isDeployment = true;
    console.log('Type event', event);
    setType(oldValues => ({
      ...oldValues,
      [event.target.name]: typeVal
    }));
    if (event.target.value !== 10) {
      isDeployment = false;
    }
    setQuery(prevState => ({
      ...prevState,
      isDeployment: isDeployment
    }));
  }

  function handleFilterToggle(event) {
    console.log('Filter event', event);
    setAdvBar(prevState => ({
      show: !prevState.show
    }));
  }

  const handleFactionChange = name => event => {
    console.log(name, event.target);
    setQuery(prevState => ({
      ...prevState,
      [name]: event.target.checked
    }));
    //setState({ ...state, [name]: event.target.checked });
  };

  const [anchorElFaction, setAnchorElFaction] = React.useState(null);
  const [anchorElSort, setAnchorElSort] = React.useState(null);
  const [anchorElOrder, setAnchorElOrder] = React.useState(null);

  const [queryParam, setQuery] = React.useState({
    searchStr: '',
    isDeployment: true,
    isRebel: true,
    isMerc: true,
    isImp: true,
    isAny: true,
    sortOrder: SortOrderEnum.DESC, // (Asc, desc)
    searchBy: SortByEnum.COST // (Name, Cost, Health)
  });

  function handleSearchUpdate(event) {
    event.persist();
    console.log(event.target.value);
    setQuery(prevState => ({
      ...prevState,
      searchStr: event.target.value
    }));
  }

  useEffect(() => {
    props.onQueryUpdate(queryParam);
  }, [queryParam]);

  function handleFactionClick(event) {
    setAnchorElFaction(event.currentTarget);
  }

  function handleFactionClose() {
    setAnchorElFaction(null);
  }

  function handleSortClick(event) {
    setAnchorElSort(event.currentTarget);
  }

  function handleSortClose() {
    setAnchorElSort(null);
  }

  // TODO: Update parent component everytime the query updates
  /**
   *
   * {
   *      searchStr: string
   *      isDeployment: bool
   *      isRebel: bool
   *      isMerc: bool
   *      isImp: bool
   *      isAny: bool
   *      sortOrder: enum (Asc, desc)
   *      searchBy: enum (Name, Cost, Health)
   *
   * }
   *
   */

  return (
    <div className="searchDiv">
      <Paper className={classes.root}>
        <InputBase
          className={classes.input}
          placeholder="Name"
          onChange={handleSearchUpdate}
          inputProps={{ 'aria-label': 'Search By' }}
        />
        <IconButton className={classes.iconButton} aria-label="Search">
          <SearchIcon />
        </IconButton>
        <Divider className={classes.divider} />
        <Select
          value={type.value}
          onChange={handleTypeChange}
          inputProps={{
            name: 'value',
            id: 'value-simple'
          }}
        >
          <MenuItem value={10}>Deployment</MenuItem>
          <MenuItem value={20}>Command</MenuItem>
        </Select>
        <Divider className={classes.divider} />
        <IconButton
          className={classes.iconButton}
          aria-label="Filter"
          onClick={handleFilterToggle}
        >
          <FilterIcon />
        </IconButton>
      </Paper>
      {/** Advanced */}
      {showAdvBar.show ? (
        <div className="searchDiv searchAdvDiv">
          <List
            className="searchAdvList"
            component="nav"
            aria-label="main mailbox folders"
          >
            <ListItem className="sortRadioList">
              <div>
                <Radio
                  checked={selectedValue === SortRadioEnum.COST_HIGH}
                  onChange={handleSortChange}
                  value={SortRadioEnum.COST_HIGH}
                  name="radio-button-sort"
                  inputProps={{ 'aria-label': 'A' }}
                />
              </div>
              <div>Cost: High to Low</div>
            </ListItem>
            <ListItem className="sortRadioList">
              <div>
                <Radio
                  checked={selectedValue === SortRadioEnum.COST_LOW}
                  onChange={handleSortChange}
                  value={SortRadioEnum.COST_LOW}
                  name="radio-button-sort"
                  inputProps={{ 'aria-label': 'B' }}
                />
              </div>
              <div>Cost: Low to High</div>
            </ListItem>
            <ListItem className="sortRadioList">
              <div>
                <Radio
                  checked={selectedValue === SortRadioEnum.NAME_HIGH}
                  onChange={handleSortChange}
                  value={SortRadioEnum.NAME_HIGH}
                  name="radio-button-sort"
                  inputProps={{ 'aria-label': 'C' }}
                />
              </div>
              <div>Name: A to Z</div>
            </ListItem>
            <ListItem className="sortRadioList">
              <div>
                <Radio
                  checked={selectedValue === SortRadioEnum.NAME_LOW}
                  onChange={handleSortChange}
                  value={SortRadioEnum.NAME_LOW}
                  name="radio-button-sort"
                  inputProps={{ 'aria-label': 'D' }}
                />
              </div>
              <div>Name: Z to A</div>
            </ListItem>
            <Divider />
            <ListItem button>
              <ListItemText primary="Trash" />
            </ListItem>
            <ListItemLink href="#simple-list">
              <FormControlLabel
                control={
                  <Checkbox
                    checked={queryParam.isRebel}
                    onChange={handleFactionChange('isRebel')}
                    value="isRebel"
                  />
                }
                label="Rebel"
              />
            </ListItemLink>
            <ListItemLink href="#simple-list">
              <FormControlLabel
                control={
                  <Checkbox
                    checked={queryParam.isImp}
                    onChange={handleFactionChange('isImp')}
                    value="isImp"
                  />
                }
                label="Imperial"
              />
            </ListItemLink>
            <ListItemLink href="#simple-list">
              <FormControlLabel
                control={
                  <Checkbox
                    checked={queryParam.isMerc}
                    onChange={handleFactionChange('isMerc')}
                    value="isMerc"
                  />
                }
                label="Merc"
              />
            </ListItemLink>
            <ListItemLink href="#simple-list">
              <FormControlLabel
                control={
                  <Checkbox
                    checked={queryParam.isAny}
                    onChange={handleFactionChange('isAny')}
                    value="isAny"
                  />
                }
                label="Any"
              />
            </ListItemLink>
          </List>
          <Divider />
          <Paper className={classes.advRoot}>
            <div>
              <Button
                aria-controls="customized-menu"
                aria-haspopup="true"
                variant="contained"
                color="primary"
                onClick={handleFactionClick}
              >
                Faction
              </Button>
              <StyledMenu
                id="customized-menu"
                anchorEl={anchorElFaction}
                keepMounted
                open={Boolean(anchorElFaction)}
                onClose={handleFactionClose}
              >
                <StyledMenuItem>
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={queryParam.isRebel}
                        onChange={handleFactionChange('isRebel')}
                        value="isRebel"
                      />
                    }
                    label="Rebel"
                  />
                </StyledMenuItem>
                <StyledMenuItem>
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={queryParam.isMerc}
                        onChange={handleFactionChange('isMerc')}
                        value="isMerc"
                      />
                    }
                    label="Merc"
                  />
                </StyledMenuItem>
                <StyledMenuItem>
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={queryParam.isImp}
                        onChange={handleFactionChange('isImp')}
                        value="isImp"
                      />
                    }
                    label="Imperial"
                  />
                </StyledMenuItem>
                <StyledMenuItem>
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={queryParam.isAny}
                        onChange={handleFactionChange('isAny')}
                        value="isAny"
                      />
                    }
                    label="Any"
                  />
                </StyledMenuItem>
              </StyledMenu>
            </div>
            <Divider className={classes.divider} />
            {/** Sort */}
            <div>
              <Button
                aria-controls="customized-menu"
                aria-haspopup="true"
                variant="contained"
                color="primary"
                onClick={handleSortClick}
              >
                Sort
              </Button>
              <StyledMenu
                id="customized-menu"
                anchorEl={anchorElSort}
                keepMounted
                open={Boolean(anchorElSort)}
                onClose={handleSortClose}
              >
                <StyledMenuItem>
                  <ListItemIcon>
                    <SendIcon />
                  </ListItemIcon>
                  <ListItemText primary="Name" />
                </StyledMenuItem>
                <StyledMenuItem>
                  <ListItemIcon>
                    <DraftsIcon />
                  </ListItemIcon>
                  <ListItemText primary="Cost" />
                </StyledMenuItem>
                <StyledMenuItem>
                  <ListItemIcon>
                    <InboxIcon />
                  </ListItemIcon>
                  <ListItemText primary="Health" />
                </StyledMenuItem>
                <StyledMenuItem>
                  <ListItemIcon>
                    <InboxIcon />
                  </ListItemIcon>
                  <ListItemText primary="Speed" />
                </StyledMenuItem>
              </StyledMenu>
            </div>
            <Divider className={classes.divider} />
          </Paper>
        </div>
      ) : null}
    </div>
  );
}
