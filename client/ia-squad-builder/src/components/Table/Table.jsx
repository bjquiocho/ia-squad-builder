/*!

=========================================================
* Material Dashboard React - v1.7.0
=========================================================

* Product Page: https://www.creative-tim.com/product/material-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/material-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from 'react';
import clsx from 'clsx';

import PropTypes from 'prop-types';
// @material-ui/core components
import withStyles from '@material-ui/core/styles/withStyles';
import Table from '@material-ui/core/Table';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
// core components
import tableStyle from 'assets/jss/material-dashboard-react/components/tableStyle.jsx';
import { Button } from '@material-ui/core';
import Icon from '@material-ui/core/Icon';
import PreviewDialog from 'components/Dialog/PreviewDialog';

function CustomTable({ ...props }) {
  const { classes, tableHead, tableData, tableHeaderColor, addCard } = props;

  // Return the correct faction font awesome icon
  const getFactionIcon = faction => {
    switch (faction) {
      case 'rebel':
        return 'fab fa-rebel';
      case 'imperial':
        return 'fab fa-empire';
      case 'mercenary':
        return 'fab fa-mandalorian';
      default:
        return 'any';
    }
  };

  const [selectedValue, setSelectedValue] = React.useState({});

  function openDialog(imgUrl, setsArr) {
    console.log('Open dialog', imgUrl);
    console.log('Sets', setsArr);
    let value = {
      url: imgUrl,
      sets: setsArr
    };
    setSelectedValue(value);
    handleClickOpen();
  }

  const [open, setOpen] = React.useState(false);

  function handleClickOpen() {
    setOpen(true);
  }

  function addToList(id) {
    console.log('Adding', id);
    addCard(id);
  }

  const handleClose = value => {
    setOpen(false);
  };

  return (
    <div className={classes.tableResponsive}>
      <Table className={classes.table} size="small">
        <colgroup>
          <col width="10%" />
          <col width="10%" />
          <col width="60%" />
          <col width="10%" />
          <col width="10%" />
        </colgroup>
        <TableBody>
          {tableData.map((prop, key) => {
            let deployment = prop.deployment;
            let factionIcon = getFactionIcon(deployment.faction);
            let costIcon = deployment.elite
              ? 'costIcon elite'
              : 'costIcon nonElite';

            return (
              <TableRow key={key}>
                <TableCell align="center" scope="row" padding="none">
                  {factionIcon === 'any' ? (
                    <img src="https://tabletopadmiral.com/static/main/images/Neutral.png" />
                  ) : (
                    <Icon className={clsx(classes.icon, factionIcon)} />
                  )}
                </TableCell>
                <TableCell align="center" className="costTableCell">
                  <div className={costIcon}>{deployment.deploymentCost}</div>
                </TableCell>
                <TableCell align="left" padding="none">
                  {deployment.unique ? (
                    <Icon
                      className={clsx(
                        classes.icon,
                        'fa fa-square-full uniqueIcon'
                      )}
                    />
                  ) : null}{' '}
                  &nbsp;
                  {deployment.name}
                </TableCell>
                <TableCell align="center" padding="none">
                  <img
                    src={deployment.imageurl}
                    className="previewImg"
                    onClick={() =>
                      openDialog(deployment.imageurl, deployment.sets)
                    }
                  ></img>
                </TableCell>
                <TableCell align="center" padding="none">
                  <Button onClick={() => addToList(deployment.iaspecName)}>
                    <Icon
                      className={clsx(classes.icon, 'fa fa-plus addCardIcon')}
                    />
                  </Button>
                </TableCell>
              </TableRow>
            );
          })}
        </TableBody>
      </Table>
      <PreviewDialog
        selectedValue={selectedValue}
        open={open}
        onClose={handleClose}
      />
    </div>
  );
}

CustomTable.defaultProps = {
  tableHeaderColor: 'gray'
};

CustomTable.propTypes = {
  classes: PropTypes.object.isRequired,
  tableHeaderColor: PropTypes.oneOf([
    'warning',
    'primary',
    'danger',
    'success',
    'info',
    'rose',
    'gray'
  ]),
  tableHead: PropTypes.arrayOf(PropTypes.string)
  // tableData: PropTypes.arrayOf(PropTypes.arrayOf(PropTypes.string))
};

export default withStyles(tableStyle)(CustomTable);
