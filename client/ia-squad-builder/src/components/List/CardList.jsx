import React from 'react';
import clsx from 'clsx';

import { makeStyles, useTheme } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Grid from '@material-ui/core/Grid';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import Divider from '@material-ui/core/Divider';
import Box from '@material-ui/core/Box';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Icon from '@material-ui/core/Icon';
import PreviewDialog from 'components/Dialog/PreviewDialog';

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(3, 2)
  }
}));

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <Typography
      component="div"
      role="tabpanel"
      hidden={value !== index}
      id={`action-tabpanel-${index}`}
      aria-labelledby={`action-tab-${index}`}
      {...other}
    >
      <Box p={3}>{children}</Box>
    </Typography>
  );
}

function a11yProps(index) {
  return {
    id: `action-tab-${index}`,
    'aria-controls': `action-tabpanel-${index}`
  };
}

export default function CardList({ ...props }) {
  const classes = useStyles();
  const { deploymentList, deploymentInfo, commandList } = props;
  const theme = useTheme();

  const [value, setValue] = React.useState(0);

  function handleChange(event, newValue) {
    setValue(newValue);
  }

  function handleChangeIndex(index) {
    setValue(index);
  }

  const [open, setOpen] = React.useState(false);

  function handleClickOpen() {
    setOpen(true);
  }

  const [selectedValue, setSelectedValue] = React.useState({});

  function openDialog(imgUrl, setsArr) {
    console.log('Open dialog', imgUrl);
    let value = {
      url: imgUrl,
      sets: setsArr
    };
    setSelectedValue(value);
    handleClickOpen();
  }

  const handleClose = value => {
    setOpen(false);
  };

  return (
    <div>
      <Paper className="customBar">
        <AppBar position="static">
          <Toolbar>
            <Tabs
              value={value}
              onChange={handleChange}
              indicatorColor="primary"
              textColor="primary"
              aria-label="action tabs example"
            >
              <Tab
                label={
                  <React.Fragment>
                    <div>
                      <Icon className={clsx('fa fa-users')} />
                      <span className="cardCostInfo">
                        {'[' + deploymentInfo.deploymentCost + '/40]'}
                      </span>
                    </div>
                  </React.Fragment>
                }
                {...a11yProps(0)}
              />
              <Tab
                label={
                  <React.Fragment>
                    <div>
                      <Icon className={clsx('fa fa-copy')} />
                      <span className="cardCostInfo">
                        {'[' + deploymentInfo.deploymentCost + '/15]'}
                      </span>
                    </div>
                  </React.Fragment>
                }
                {...a11yProps(1)}
              />
            </Tabs>
            <IconButton
              edge="start"
              className={classes.menuButton}
              color="inherit"
              aria-label="menu"
            >
              <MenuIcon />
            </IconButton>
          </Toolbar>
        </AppBar>
        <Grid container className={classes.root} spacing={0}>
          <Grid item xs={3}>
            <Paper className="listInfo">
              Activations: {deploymentInfo.activationCount}
            </Paper>
            <Paper className="listInfo">
              Health: {deploymentInfo.healthCount}
            </Paper>
            <Paper className="listInfo">
              Figures: {deploymentInfo.figureCount}
            </Paper>
            {deploymentInfo.restrictionHash
              ? Object.keys(deploymentInfo.restrictionHash).map(key => (
                  <Paper key={key} className="listInfo">
                    {key}: {deploymentInfo.restrictionHash[key]}
                  </Paper>
                ))
              : null}{' '}
          </Grid>
          <Grid item xs={9}>
            <Paper className={classes.paper}>
              <TabPanel value={value} index={0}>
                {deploymentList.length !== 0 ? (
                  deploymentList.map((card, index) => (
                    <img
                      key={index}
                      src={card.imageurl}
                      className="deploymentCardListImg"
                      onClick={() => openDialog(card.imageurl, card.sets)}
                    />
                  ))
                ) : (
                  <div>Please add to your squad</div>
                )}{' '}
              </TabPanel>
              <TabPanel value={value} index={1}>
                {commandList.length !== 0 ? (
                  commandList.map((card, index) => (
                    <img
                      key={index}
                      src={card.imageurl}
                      className="commandCardListImg"
                      onClick={() => openDialog(card.imageurl, [])}
                    />
                  ))
                ) : (
                  <div>Please add to your squad</div>
                )}{' '}
              </TabPanel>
            </Paper>
          </Grid>
        </Grid>
      </Paper>
      <PreviewDialog
        selectedValue={selectedValue}
        open={open}
        onClose={handleClose}
      />
    </div>
  );
}
