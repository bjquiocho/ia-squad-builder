import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Avatar from '@material-ui/core/Avatar';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemText from '@material-ui/core/ListItemText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Dialog from '@material-ui/core/Dialog';
import PersonIcon from '@material-ui/icons/Person';
import AddIcon from '@material-ui/icons/Add';
import Typography from '@material-ui/core/Typography';
import { blue } from '@material-ui/core/colors';

const emails = ['username@gmail.com', 'user02@gmail.com'];
const useStyles = makeStyles({
  avatar: {
    backgroundColor: blue[100],
    color: blue[600]
  },
  bigAvatar: {
    width: 100,
    height: 100,
    borderRadius: 10
  },
  avatarImg: {
    width: 'inherit'
  },
  listItemAvatar: {
    paddingRight: 10
  }
});

function PreviewDialog(props) {
  const classes = useStyles();
  const { onClose, selectedValue, open } = props;

  useEffect(() => {
    console.log('Component did update', selectedValue);
    let setCount = 0;
    if (selectedValue.sets) {
      Object.keys(selectedValue.sets).map(function(keyName) {
        setCount++;
      });
    }
    updateSetCount(setCount);
  }, [selectedValue]);

  const [setCount, updateSetCount] = React.useState(0);

  function handleClose() {
    onClose(selectedValue);
  }

  function handleListItemClick(value) {
    onClose(value);
  }

  return (
    <Dialog
      onClose={handleClose}
      aria-labelledby="simple-dialog-title"
      open={open}
      maxWidth="xs"
      BackdropProps={{
        classes: {
          root: classes.root
        }
      }}
    >
      {/* <DialogTitle id="simple-dialog-title">Set backup account</DialogTitle> */}
      <img src={selectedValue.url} className="previewImgDialog" />
      <List>
        {setCount !== 0 ? (
          <ListItem>
            <ListItemText primary="Found in set(s):" />
          </ListItem>
        ) : null}
        {selectedValue.sets
          ? Object.keys(selectedValue.sets).map(keyName => (
              // use keyName to get current key's name
              // and selectedValue.sets[keyName] to get its value
              <ListItem key={keyName}>
                <ListItemAvatar className={classes.listItemAvatar}>
                  <Avatar className={classes.bigAvatar}>
                    <img
                      className={classes.avatarImg}
                      src={selectedValue.sets[keyName]}
                    />
                  </Avatar>
                </ListItemAvatar>
                <ListItemText primary={keyName} />
                {/* <img src={selectedValue.sets[keyName]} /> */}
              </ListItem>
            ))
          : null}
      </List>
    </Dialog>
  );
}

PreviewDialog.propTypes = {
  onClose: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
  selectedValue: PropTypes.object.isRequired
};

export default PreviewDialog;
