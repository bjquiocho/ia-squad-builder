/*!

=========================================================
* Material Dashboard React - v1.7.0
=========================================================

* Product Page: https://www.creative-tim.com/product/material-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/material-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from 'react';
// nodejs library to set properties for components
import PropTypes from 'prop-types';
// react plugin for creating charts
import ChartistGraph from 'react-chartist';
// @material-ui/core
import withStyles from '@material-ui/core/styles/withStyles';
import TextField from '@material-ui/core/TextField';
import Icon from '@material-ui/core/Icon';
// @material-ui/icons
import BugReport from '@material-ui/icons/BugReport';
import Code from '@material-ui/icons/Code';
import Cloud from '@material-ui/icons/Cloud';
// core components
import GridItem from 'components/Grid/GridItem.jsx';
import GridContainer from 'components/Grid/GridContainer.jsx';
import Table from 'components/Table/Table.jsx';
import Tasks from 'components/Tasks/Tasks.jsx';
import CustomTabs from 'components/CustomTabs/CustomTabs.jsx';
import CustomInput from 'components/CustomInput/CustomInput.jsx';
import SearchInput from 'components/SearchInput/SearchInput.jsx';

import Card from 'components/Card/Card.jsx';
import CardHeader from 'components/Card/CardHeader.jsx';
import CardBody from 'components/Card/CardBody.jsx';
import DeploymentData from '../../assets/data/deploymentCards.json';
import CommandData from '../../assets/data/commandCards.json';
import CardList from 'components/List/CardList.jsx';
// import styles from '../../assets/css/custom-material.css';

import { bugs, website, server } from 'variables/general.jsx';

import dashboardStyle from 'assets/jss/material-dashboard-react/views/dashboardStyle.jsx';
import DeploymentTable from 'components/Table/DeploymentTable.jsx';
import CommandTable from 'components/Table/CommandTable.jsx';

// We can inject some CSS into the DOM.
const styles = {
  root: {
    color: 'white'
  }
};

const deploymentCards = DeploymentData;
const commandCards = CommandData;

const deploymentHash = {};
const commandHash = {};

function Enum(constantsList) {
  for (var i in constantsList) {
    this[constantsList[i]] = i;
  }
}

const SortByEnum = new Enum(['NAME', 'COST', 'HEALTH', 'SPEED']);
const SortOrderEnum = new Enum(['ASC', 'DESC']);

class Dashboard extends React.Component {
  state = {
    value: 0,
    queryParam: {
      searchStr: '',
      isDeployment: true,
      isRebel: true,
      isMerc: true,
      isImp: true,
      isAny: true,
      sortOrder: 1, // (Asc, desc)
      searchBy: 1 // (Name, Cost, Health)
    },
    dataTableArr: [],
    deploymentList: [],
    commandList: [],
    deploymentInfo: {
      deploymentCost: 0,
      activationCount: 0,
      healthCount: 0,
      figureCount: 0,
      restrictionHash: []
    }
  };

  componentDidMount() {
    // Build our deployment hash
    deploymentCards.forEach(card => {
      deploymentHash[card.deployment.iaspecName] = card.deployment;
    });
    console.log('Our Deployment hash', deploymentHash);
    // Build our command hash
    commandCards.forEach(element => {
      commandHash[element.card.iaspecName] = element.card;
    });
    console.log('Our Command hash', commandHash);
  }

  handleChange = (event, value) => {
    this.setState({ value });
  };

  handleChangeIndex = index => {
    this.setState({ value: index });
  };

  handleQueryParams = queryParams => {
    console.log('Got New query', queryParams);
    this.setState({ queryParam: queryParams });
    this.updateDatatable(queryParams);
  };

  addCardToList = card => {
    console.log('Adding card', card);
    let tempList = [];
    if (this.state.queryParam.isDeployment) {
      tempList = this.state.deploymentList;
      tempList.push(deploymentHash[card]);
      this.updateDeploymentInfo(tempList);
      this.setState({ deploymentList: tempList });
      console.log(tempList);
    } else {
      tempList = this.state.commandList;
      tempList.push(commandHash[card]);
      this.setState({ commandList: tempList });
    }
    // this.setState({ password: e.target.value })
  };

  updateDeploymentInfo(deploymentList) {
    let tempDeploymentInfo = {};
    let restrictionHash = {};
    // Compute deployment cost
    let deploymentCost = 0;
    // Compute total number of activations
    let activationCount = 0;
    // Compute total health
    let healthCount = 0;
    // Compute total number of figures
    let figureCount = 0;

    deploymentList.forEach(deployment => {
      if (deployment.unitsInGroup !== 0) {
        activationCount += 1;
      }

      let healthModifier = 1;
      if (deployment.unitsInGroup > 1) {
        healthModifier = deployment.unitsInGroup;
      }

      healthCount += deployment.health * healthModifier;

      figureCount += deployment.unitsInGroup;

      deploymentCost += deployment.deploymentCost;

      // add restrictions to our hash
      if (deployment.restrictions) {
        deployment.restrictions.forEach(res => {
          let restriction = res.charAt(0).toUpperCase() + res.slice(1) + 's';
          if (restrictionHash[restriction] === undefined) {
            restrictionHash[restriction] = 1;
          } else {
            restrictionHash[restriction] = restrictionHash[restriction] + 1;
          }
        });
      }
    });

    console.log(restrictionHash);

    tempDeploymentInfo = {
      deploymentCost: deploymentCost,
      activationCount: activationCount,
      healthCount: healthCount,
      figureCount: figureCount,
      restrictionHash: restrictionHash
    };

    this.setState({ deploymentInfo: tempDeploymentInfo });
  }

  /**
   * Filter all objects based on Type and Color filters.
   * The type is always a single type, or an empty string.
   * The colors are an array of one or multiple colors.
   * TODO: Implement URL writing for multiple colors, e.g. /colors/red+blue
   * @param {Array}  arrObjects The list of objects to filter.
   * @param {String} strType     A single object type (car, train, airplane)
   * @param {Array}  arrColors   An array of color strings
   */
  updateDatatable(queryParams) {
    let filteredCards = [];
    if (!queryParams.isDeployment) {
      filteredCards = this.searchCommand(queryParams);
      // Sort the cards
      filteredCards = this.sortCommand(
        filteredCards,
        queryParams.searchBy,
        queryParams.sortOrder
      );
    } else {
      filteredCards = this.searchDeployment(queryParams);
      // Sort the cards
      filteredCards = this.sortDeployment(
        filteredCards,
        queryParams.searchBy,
        queryParams.sortOrder
      );
    }

    this.setState({ dataTableArr: filteredCards });
  }

  searchDeployment(queryParams) {
    let queryStr = queryParams.searchStr.toLowerCase();
    let tempCards = deploymentCards;
    // Filter array based on query
    return tempCards.filter(element => {
      // Check faction first since it is not as expensive to search string
      let elementFaction = element.deployment.faction;
      if (
        (elementFaction === 'any' && !queryParams.isAny) ||
        (elementFaction === 'rebel' && !queryParams.isRebel) ||
        (elementFaction === 'mercenary' && !queryParams.isMerc) ||
        (elementFaction === 'imperial' && !queryParams.isImp)
      ) {
        return false;
      }

      // Search string by Name
      let querySearchBy = element.deployment.name.toLowerCase();

      // Add tags to search by
      let elementTags = element.deployment.tags
        ? element.deployment.tags.join()
        : '';
      if (
        !querySearchBy.includes(queryStr) &&
        !elementTags.includes(queryStr)
      ) {
        return false;
      }

      return true;
    });
  }

  searchCommand(queryParams) {
    let queryStr = queryParams.searchStr.toLowerCase();
    let tempCards = commandCards;
    // Filter array based on query
    return tempCards.filter(element => {
      // Check faction first since it is not as expensive to search string
      let elementFaction = element.card.faction;
      if (
        (elementFaction === 'any' && !queryParams.isAny) ||
        (elementFaction === 'rebel' && !queryParams.isRebel) ||
        (elementFaction === 'mercenary' && !queryParams.isMerc) ||
        (elementFaction === 'imperial' && !queryParams.isImp)
      ) {
        console.log('Filter out faction');
        return false;
      }

      // Search string by Name
      let querySearchBy = element.card.name.toLowerCase();
      if (!querySearchBy.includes(queryStr)) {
        return false;
      }
      return true;
    });
  }

  sortDeployment(cards, searchBy, sortOrder) {
    let sortedCards;

    if (searchBy === SortByEnum.NAME) {
      sortedCards = cards.sort(function(a, b) {
        var textA = a.deployment.name.toLowerCase();
        var textB = b.deployment.name.toLowerCase();
        if (sortOrder === SortOrderEnum.DESC) {
          return textA < textB ? -1 : textA > textB ? 1 : 0;
        } else {
          return textA > textB ? -1 : textA < textB ? 1 : 0;
        }
      });
    } else {
      sortedCards = cards.sort(function(a, b) {
        if (sortOrder === SortOrderEnum.DESC) {
          return b.deployment.deploymentCost - a.deployment.deploymentCost;
        } else {
          return a.deployment.deploymentCost - b.deployment.deploymentCost;
        }
      });
    }

    return sortedCards;
  }

  sortCommand(cards, searchBy, sortOrder) {
    let sortedCards;

    if (searchBy === SortByEnum.NAME) {
      sortedCards = cards.sort(function(a, b) {
        var textA = a.card.name.toLowerCase();
        var textB = b.card.name.toLowerCase();
        if (sortOrder === SortOrderEnum.DESC) {
          return textA < textB ? -1 : textA > textB ? 1 : 0;
        } else {
          return textA > textB ? -1 : textA < textB ? 1 : 0;
        }
      });
    } else {
      sortedCards = cards.sort(function(a, b) {
        if (sortOrder === SortOrderEnum.DESC) {
          return b.card.cost - a.card.cost;
        } else {
          return a.card.cost - b.card.cost;
        }
      });
    }

    return sortedCards;
  }

  render() {
    const { classes } = this.props;
    return (
      <div>
        <GridContainer>
          {/** Deck Component */}
          <GridItem xs={12} sm={12} md={5}>
            <Card plain>
              <CardBody className="searchContainer">
                <SearchInput onQueryUpdate={this.handleQueryParams} />
                {this.state.queryParam.isDeployment ? (
                  <DeploymentTable
                    tableHeaderColor="warning"
                    tableData={this.state.dataTableArr}
                    addCard={this.addCardToList}
                  />
                ) : (
                  <CommandTable
                    tableHeaderColor="warning"
                    tableData={this.state.dataTableArr}
                    addCard={this.addCardToList}
                  />
                )}
              </CardBody>
            </Card>
          </GridItem>
          {/** Search Component */}
          <GridItem xs={12} sm={12} md={7}>
            <Card plain>
              <CardBody>
                <CardList
                  deploymentList={this.state.deploymentList}
                  deploymentInfo={this.state.deploymentInfo}
                  commandList={this.state.commandList}
                />
              </CardBody>
            </Card>
          </GridItem>
        </GridContainer>
      </div>
    );
  }
}

Dashboard.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(dashboardStyle)(Dashboard);
